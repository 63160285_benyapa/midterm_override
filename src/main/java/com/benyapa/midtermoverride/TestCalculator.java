/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.midtermoverride;

/**
 *
 * @author bwstx
 */
public class TestCalculator {
    public static void main(String[] args) {
        //Plus
        Plus plus1 = new Plus(2, 5);
        System.out.println(plus1.calResult());
        plus1.printLine();
        
        //Minus
        Minus minus1 = new Minus(3, 9);
        System.out.println(minus1.calResult());
        minus1.printLine();
        
        //Multiplied
        Multiplied multi1 =new Multiplied(6,9);
        System.out.println(multi1.calResult());
        multi1.printLine();
        
        //Devide
        Devide devide = new Devide(3,5);
        System.out.println(devide.calResult());
        devide.printLine();
    }
    
}
